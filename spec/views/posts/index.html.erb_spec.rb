# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'posts/index', type: :view do
  let!(:user) { create(:user) }
  before(:each) do
    assign(:posts, [
             Post.create!(
               user: user,
               title: 'Title',
               description: 'MyText'
             ),
             Post.create!(
               user: user,
               title: 'Title',
               description: 'MyText'
             )
           ])
  end

  it 'renders a list of posts' do
    render
    assert_select 'tr>td', text: 'Title'.to_s, count: 2
    assert_select 'tr>td', text: 'MyText'.to_s, count: 2
  end
end
